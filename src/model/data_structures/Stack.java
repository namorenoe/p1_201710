package model.data_structures;

public class Stack <T> {

	private GenericNode<T> primerNodo;

	public Stack (){
		primerNodo = null;
	}

	public void push (T item){
		GenericNode<T> add = new GenericNode<>();
		add.setItem(item);
		add.setSiguiente(primerNodo);
		primerNodo = add;
	}

	public T pop(){

		T res = null;

		if(!isEmpty()){
			res = primerNodo.darItem();
			primerNodo = primerNodo.darSiguiente();
		}


		return res;
	}

	public boolean isEmpty(){
		boolean res = true;

		if(primerNodo != null) res=false;
		else res = true;

		return res;
	}

	public int size(){
		int res = 0;

		GenericNode<T> nodo = primerNodo;

		while(nodo!=null){
			res++;
			nodo=nodo.darSiguiente();
		}

		return res;
	}
}
